local mymodule = {}

mymodule.default_action = "status"

function mymodule.status(self)
	return self.model.getstatus()
end

function mymodule.startstop(self)
	return self.handle_form(self, self.model.get_startstop, self.model.startstop_service, self.clientdata)
end

function mymodule.details(self)
	return self.model.getstatusdetails()
end

function mymodule.listfiles(self)
	return self.model.getfilelist()
end

function mymodule.expert(self)
	return self.handle_form(self, self.model.getfiledetails, self.model.updatefiledetails, self.clientdata, "Save", "Edit ClamAV File", "File Saved")
end

function mymodule.logfile(self)
	return self.model.get_logfile(self, self.clientdata)
end

return mymodule
